## Lua/Luajit OpenBox Application Pipe-Menu
--

This is a tool to generate OpenBox-Menus

Currently is adds icons for Categories from the Adwaita Theme
Currently is adds icons for Applications from the hicolor Theme


#### Table of contents
* [Characteristics](#characteristics)
* [Requirements](#requirements)

### Characteristics:
----
#####  How to Generate menus:

Just execute the tool in the command line( with a redirect, if you want a static menu ), or include it in ~/config/openbox/menu.xml file..

 * standalone    
   ```lua
   /usr/bin/lobamenu
   ```
 * via menu.xml integration
   ```lua
   <menu execute="/usr/bin/lobamenu" id="applications-menu" icon="/usr/share/pixmaps/other/Applications.png" label="Applications"/>
   ```

## Requirements:
----
To use lobamenu, you just need to install its depedencies..

### Devuan/Debian

###### 1. Install Dependencies.
 * ```lua
   apt-get install lua5.3
   update-alternatives --install /usr/bin/lua lua /usr/bin/lua5.3 1
   update-alternatives --install /usr/bin/luac luac /usr/bin/luac5.3 1
   ```
